## Description

(Add a short description about what this new page covers)

## References

(Link related Design System or GitLab Design issues)

## Checklist

* [ ] Page link with corresponding status added to `getting-started/status.vue`
* [ ] Check if cross-reference to this page from other pages might be relevant or helpful
<!-- For pages outside components only (NOT pages/components/) -->
* [ ] Page link added to `layout/default.vue`
<!-- For component pages only (pages/components/) -->
* [ ] Depending on type of the page, content should follow the documentation template
  * [Component page template](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/blob/master/pages/components/template.vue)
